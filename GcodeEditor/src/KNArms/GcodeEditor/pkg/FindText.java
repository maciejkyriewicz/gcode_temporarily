/*
 * Klasa odpowiedzialna za wyszukiwanie danego wyrazu w JTextArea
 */
package KNArms.GcodeEditor.pkg;



import java.awt.Color;
import javax.swing.*;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.Highlighter;
import javax.swing.text.JTextComponent;
/**
 *
 * @author Maciej Tkacz
 */
public class FindText 
{
    
   
    private Highlighter.HighlightPainter myHighlighterPainter =new MyHighlightPainter(Color.GREEN);
    
   
    public FindText()
    {
      
    }
    public void highligh(JTextComponent textComp,String pattern) throws BadLocationException
    {
        try
        {
            Highlighter hilite=textComp.getHighlighter();
            Document doc =textComp.getDocument();
            String text=doc.getText(0,doc.getLength());
            int pos=0;
            while((pos=text.toUpperCase().indexOf(pattern.toUpperCase(),pos))>-1)
            {
                hilite.addHighlight(pos,pos+pattern.length(),myHighlighterPainter);
                pos+=pattern.length();
                    
            }
            
        }
        catch(Exception e)
        {
            
        }       
    }
    public void removehighlights(JTextComponent textComp)
    {
        Highlighter hilite=textComp.getHighlighter();
        Highlighter.Highlight[] hilites=hilite.getHighlights();
        for (int i = 0; i < hilites.length; i++) 
        {
            
            if (hilites[i].getPainter() instanceof MyHighlightPainter)
            {   
                hilite.removeHighlight(hilites[i]);
            }
            
        }
            
    }  
}
  

