/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KNArms.GcodeEditor.pkg;

import javax.swing.JOptionPane;
import javax.swing.JTextArea;

/**
 *
 * @author Maciej Tkacz
 */
public class GcodeVerification 
{
    private final JTextArea text;
    private String string_text,string_compare;
    private char[] stringToCharArray ;
    private String[] table_string ;
    private Boolean state=false;
    private int count_signs,count_spaces;
    
    
    
    public GcodeVerification(JTextArea text)
    {
        this.text=text;
        string_text=text.getText();
        this.stringToCharArray = string_text.toCharArray();
        count_signs=-1;
        count_spaces=0;
       
      
    }
    public void check_gcode()
    {
        //NA DANA CHWILE KOD TEN ROZROZNIA LINIE GCODE JAK I LINIE KOMENTARZA WYPISUJAC BLAD PRZEZ MESSAGEBOX
       string_text=text.getText();
       this.stringToCharArray = string_text.toCharArray();
      
       for (int i=0;i<stringToCharArray.length;i++)
       {
            
                if (stringToCharArray[i]=='%')
               {
                 state=true; 
                 
               }
               else if ((stringToCharArray[i]=='G')||(stringToCharArray[i]=='M'))
               {
                 state=true;  
                 
               }
               else if ((!(stringToCharArray[i]=='G')||!(stringToCharArray[i]=='M')||!(stringToCharArray[i]=='%'))&&(state==false))
               {
                   
                   JOptionPane.showMessageDialog(null,"Błąd");
                   break;
               }
                if (stringToCharArray[i]=='\n')
                {
                    state=false;
                }
                
             
            
        }
    }
    public Boolean compare(String word)
    {
        //METODA POROWNUJACA KOMENDY 
        Boolean state=false;
        String[] table_string={"G01","G02"};
        for (int i=0;i<table_string.length;i++)
        {
            if (table_string[i]==word)state=true;
            
        }
        return state;
    }
}
