/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KNArms.GcodeEditor.pkg;

import static com.sun.corba.se.impl.util.Utility.printStackTrace;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTextArea;
import javax.swing.undo.UndoManager;


/**
 *
 * @author Damian
 */
public class CustomMenuBar extends JMenuBar implements ActionListener, ItemListener {
    
    private JMenu file;
    private JMenu edit;
    private JMenu help;
    
    private JMenuItem open;
    private JMenuItem save;
    private JMenuItem saveAs;
    private JMenuItem exit;
    
    private JMenuItem cut;
    private JMenuItem copy;
    private JMenuItem paste;
    private JMenuItem redo;
    private JMenuItem undo;
    private JMenuItem selectAll;
    
    private JMenuItem about;
    private JTextArea alltext;
    private UndoManager editManager;
    private String patch;
    
    
    

    
    public CustomMenuBar(JTextArea alltext,UndoManager editManager)
    {
        setPreferredSize(new Dimension(800,25));
        setMinimumSize(new Dimension(800,25));
        setUpMenus();
        setUpMenuItems();
        this.alltext=alltext;
        this.editManager=editManager;
        patch=null; 
    }

    private void setUpMenus() {
        createMenu("file");
        createMenu("edit");
        createMenu("help");
    }
    
    private void createMenu(String name)
    {
        switch(name)
        {
            case "file":
                file = new JMenu("File");
                file.setMnemonic(KeyEvent.VK_F);
                this.add(file);
                break;
            case "edit":
                edit = new JMenu("Edit");
                edit.setMnemonic(KeyEvent.VK_E);
                this.add(edit);
                break;
            case "help":
                help = new JMenu("Help");
                help.setMnemonic(KeyEvent.VK_H);
                this.add(help);
                break;
            default : break;
        }
    }

    private void setUpMenuItems() {
        createMenuItem("open");
        createMenuItem("save");
        createMenuItem("saveAs");
        createMenuItem("exit");
        
        createMenuItem("cut");
        createMenuItem("copy");
        createMenuItem("paste");
        createMenuItem("redo");
        createMenuItem("undo");
        createMenuItem("selectAll");
        
        createMenuItem("about");      
    }
    
    private void createMenuItem(String name)
    {
        switch(name)
        {
            case "open":
                open = new JMenuItem("Open");
                open.setAccelerator(KeyStroke.getKeyStroke(
                         KeyEvent.VK_O, ActionEvent.CTRL_MASK));
                open.addActionListener(this);
                file.add(open);
                break;
            case "save":
                save = new JMenuItem("Save");
                save.setAccelerator(KeyStroke.getKeyStroke(
                         KeyEvent.VK_S, ActionEvent.CTRL_MASK));
                save.addActionListener(this);
                file.add(save);
                break;
            case "saveAs":  //dodać shift
                saveAs = new JMenuItem("Save As");
                saveAs.setAccelerator(KeyStroke.getKeyStroke(
                         KeyEvent.VK_S, ActionEvent.CTRL_MASK | ActionEvent.SHIFT_MASK));
                saveAs.addActionListener(this);
                file.add(saveAs);
                break;
            case "exit":
                exit = new JMenuItem("Exit");
                exit.setAccelerator(KeyStroke.getKeyStroke(
                         KeyEvent.VK_F4, ActionEvent.ALT_MASK));
                exit.addActionListener(this);
                file.add(exit);
                break;
            
            case "cut":
                cut = new JMenuItem("Cut");
                cut.setAccelerator(KeyStroke.getKeyStroke(
                         KeyEvent.VK_X, ActionEvent.CTRL_MASK));
                cut.addActionListener(this);
                edit.add(cut);
                break;
            case "copy":
                copy = new JMenuItem("Copy");
                copy.setAccelerator(KeyStroke.getKeyStroke(
                         KeyEvent.VK_C, ActionEvent.CTRL_MASK));
                copy.addActionListener(this);
                edit.add(copy);
                break;
            case "paste":
                paste = new JMenuItem("Paste");
                paste.setAccelerator(KeyStroke.getKeyStroke(
                         KeyEvent.VK_V, ActionEvent.CTRL_MASK));
                paste.addActionListener(this);
                edit.add(paste);
                break;
            case "redo":
                redo = new JMenuItem("Redo");
                redo.setAccelerator(KeyStroke.getKeyStroke(
                         KeyEvent.VK_Y, ActionEvent.CTRL_MASK));
                redo.addActionListener(this);
                redo.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                if (editManager.canRedo()) {
                    editManager.redo();
                }
                }
                });
                edit.add(redo);
                break;
            case "undo":
                undo = new JMenuItem("Undo");
                undo.setAccelerator(KeyStroke.getKeyStroke(
                         KeyEvent.VK_Z, ActionEvent.CTRL_MASK));
                undo.addActionListener(this);
                undo.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                if (editManager.canUndo()) {
                    editManager.undo();
                }
                }
                });
                edit.add(undo);
                break;
            case "selectAll":
                selectAll = new JMenuItem("Select All");
                selectAll.setAccelerator(KeyStroke.getKeyStroke(
                         KeyEvent.VK_A, ActionEvent.CTRL_MASK));
                selectAll.addActionListener(this);
                edit.add(selectAll);
                break;
                
            case "about":
                about = new JMenuItem("About");
                //about.setAccelerator(KeyStroke.getKeyStroke(
                        // KeyEvent.VK_A, ActionEvent.CTRL_MASK));
                about.addActionListener(this);
                help.add(about);
                break;
            default: break;
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object source = e.getSource();
        JFileChooser fc=new JFileChooser();
        File file =new File("dfsf.txt");
        
        if (source == open) 
        {
            if (fc.showOpenDialog(null)==JFileChooser.APPROVE_OPTION)
            {
              file =fc.getSelectedFile();
              boolean state=true;
                
                try
                {
                    Scanner scaner =new Scanner(file);
                   
                    while(scaner.hasNext())
                    {
                        if (state)
                        {
                            alltext.setText(null);
                            state=false;
                        }
                        alltext.append(scaner.nextLine());
                       
                    }
                   
                    patch=fc.getSelectedFile().getPath();
                }
                
                catch(FileNotFoundException e1)
                {
                    e1.printStackTrace();
                }  
            }
            //JOptionPane.showMessageDialog(null,"open");
            
            
        }
        if (source == save) 
        {   
            try 
            {
                BufferedWriter out =new BufferedWriter(new FileWriter(patch));
                
                Scanner scaner =new Scanner(alltext.getText());
                     while(scaner.hasNext())
                     {
                         out.write(scaner.nextLine());
                         out.newLine();
                     }   
                out.close();   
            }  
            catch (IOException ex) 
            {
                Logger.getLogger(CustomMenuBar.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        }
        if (source == saveAs) 
        {
            
            if (fc.showSaveDialog(null)==JFileChooser.APPROVE_OPTION)
            {
                  file =fc.getSelectedFile();
                try
                {
                    PrintWriter printwriter =new PrintWriter(file);
                     Scanner scaner =new Scanner(alltext.getText());
                     while(scaner.hasNext())
                     {
                         printwriter.println(scaner.nextLine());
                         
                     }
                     printwriter.close();
                }
                catch(FileNotFoundException e1)
                {
                    e1.printStackTrace();
                }
               patch=fc.getSelectedFile().getPath();
            }
            ///JOptionPane.showMessageDialog(null,"saveAs");
            
            
        }
        if (source == exit) 
        {
            JOptionPane.showMessageDialog(null,"exit");
            //EditorWindow.closeMainWindow();
        }
        
        if (source == cut) 
        { 
            alltext.cut();
            JOptionPane.showMessageDialog(null,"cut");
           
        }
        if (source == copy) 
        { 
            alltext.copy();
            JOptionPane.showMessageDialog(null,"copy");
           
        }
        if (source == paste) 
        {
            alltext.paste();
            JOptionPane.showMessageDialog(null,"paste");
             
        }
        if (source == redo) 
        {
            JOptionPane.showMessageDialog(null,"redo");
        }
        
        if (source == undo) 
        {
            JOptionPane.showMessageDialog(null,"undo");
           
           
        }    
        if (source == selectAll) 
        {
            alltext.selectAll();
            JOptionPane.showMessageDialog(null,"selectAll");
           
        }
        
        if (source == about) 
        {
            JOptionPane.showMessageDialog(null,"about");
        }
    }

    @Override
    public void itemStateChanged(ItemEvent e) {

    }
    
      
    
  
   
}
