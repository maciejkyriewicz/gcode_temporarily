/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KNArms.GcodeEditor.pkg;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;
import javax.swing.event.UndoableEditEvent;
import javax.swing.event.UndoableEditListener;
import javax.swing.text.BadLocationException;
import javax.swing.undo.UndoManager;

/**
 *
 * @author miczyg
 */
public class EditorWindow extends JFrame 
{
    
    private CustomMenuBar menuBar;
    private JScrollPane outputScrollPane;
    private JScrollPane editorScrollPane;
    private JButton button,button_2,button_3,button_4,button_5;
    private JTextField textfield;
    private FindText findtext;
    private String temp_1,temp_2,temp_3,comunication_state="Offline";
    private boolean state=true;
   
   
    
    public EditorWindow()
    {
        setSize(800,800);
        setTitle("PUMA G-code Editor by KN Arms");
        setLayout(new BorderLayout());
        setLocationRelativeTo(null);
        setDefaultLookAndFeelDecorated(true);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }
 
    
    public void addComponents(Container pane)throws ClassNotFoundException, InstantiationException, IllegalAccessException, UnsupportedLookAndFeelException
    {
        BorderLayout bLayout = new BorderLayout();
        pane.setLayout(bLayout);
        JTextArea editorArea = new JTextArea();
        UndoManager editManager = new UndoManager();
        FindText findtext =new FindText();
        JComboBox combo=new JComboBox();
        GcodeVerification gcode_veryfication=new GcodeVerification (editorArea);
        
        this.editorScrollPane = new JScrollPane(editorArea);
        this.editorScrollPane .setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        this.editorScrollPane .setPreferredSize(new Dimension(500, 400));
        this.editorScrollPane .setMinimumSize(new Dimension(50, 50));
        pane.add(this.editorScrollPane , BorderLayout.CENTER); 
        this.outputScrollPane = new MyScrollPane(); 
        pane.add(this.outputScrollPane, BorderLayout.PAGE_END);
        this.menuBar = new CustomMenuBar(editorArea,editManager); 
        pane.add(this.menuBar, BorderLayout.PAGE_START);
         
        UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());///Kod opowiedzialny za odsługe cofania  
        JMenuItem redoEdit = new JMenuItem("Redo");                         /// i ponowiania operacji na tekście
        JMenuItem undoEdit = new JMenuItem("Undo");
        editorArea.setLineWrap(true);
        editorArea.setWrapStyleWord(true);
        editorArea.getDocument().addUndoableEditListener(
        new UndoableEditListener() {
        public void undoableEditHappened(UndoableEditEvent e) {
        editManager.addEdit(e.getEdit());
        }
        });
        editorArea.addMouseListener(new MouseAdapter() {
        @Override
        public void mouseReleased(MouseEvent e) {
        if (SwingUtilities.isRightMouseButton(e)) {
        if (editManager.canRedo()) {
            redoEdit.setEnabled(true);
        } else {
             redoEdit.setEnabled(false);
        }
        if (editManager.canUndo()) {
            undoEdit.setEnabled(true);
        } else {
            undoEdit.setEnabled(false);
        }
            JPopupMenu popup = new JPopupMenu();
            popup.add(undoEdit);
            popup.add(redoEdit);
            popup.show(e.getComponent(), e.getX(), e.getY());
        }
        }
        });
            redoEdit.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent e) {
        if (editManager.canRedo()) {
            editManager.redo();
        }
        }
        });
            undoEdit.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent e) {
        if (editManager.canUndo()) {
            editManager.undo();
        }
        }
        });
        ///Koniec  kodu odpowiedzialnego za odsługe cofania 
        // i ponowiania operacji na tekscie
        
        
        this.temp_1="0";//Kod odpowiedzalny za zagniezdzony layout i elementy w jego wnetrzu  
        this.temp_2="0";
        this.temp_3="0";
        
        this.textfield=new JTextField();
        this.button=new JButton("SZUKAJ");
        Font font =new Font("serif",Font.BOLD,13);
        button.setFont(font);
        this.button_2=new JButton("POLACZ");
        button_2.setFont(font);
        this.button_3=new JButton("SPRAWDZ");
        button_3.setFont(font);
        this.button_4=new JButton("ROZLACZ");
        button_4.setFont(font);
        this.button_5=new JButton("WYŚLIJ");
        button_5.setFont(font);
        JLabel label=new JLabel("PORT");
        label.setHorizontalAlignment(SwingConstants.CENTER);
        label.setFont(font);  
        Label label2=new Label("STATUS POLACZENIE:");
        label2.setFont(font);  
        Label label3=new Label(comunication_state);
        label3.setFont(font); 
        label3.setForeground(Color.GREEN);
        Label label4=new Label("TEMPERATURA SERW1:");
        label4.setFont(font);  
        Label label5=new Label(temp_1);
        label5.setForeground(Color.BLUE);
        label5.setFont(font);  
        Label label6=new Label("TEMPERATURA SERW2:");
        label6.setFont(font);
        Label label7=new Label(temp_2);
        label7.setFont(font);
        label7.setForeground(Color.BLUE);
        Label label8=new Label("TEMPERATURA SERW3:");
        label8.setFont(font);
        Label label9=new Label(temp_3);
        label9.setForeground(Color.BLUE);
        label9.setFont(font);
        Label label10=new Label("WERYFIKACJA GCODE:");
        label10.setFont(font);
        Label label11=new Label("START TRANSMISJI:");
        label11.setFont(font);
        Label label12=new Label("KONIEC TRANSMISJI:");
        label12.setFont(font);
        Label label13=new Label("WYSLANIE GCODE:");
        label13.setFont(font);
        Serial_port serialport=new Serial_port(combo,editorArea);
        
         button.addActionListener(new ActionListener() {//Okodowanie przycisku SZUKAJ
        public void actionPerformed(ActionEvent e) {
            try {
                if(state)
                {
                    if (textfield.getText().isEmpty())
                    {
                        
                       JOptionPane.showMessageDialog(null,"WPROWADZ TEKST KTORY CHESZ WYSZUKAC");    
                    }
                    else
                    {
                        
                        findtext.highligh(editorArea,textfield.getText());
                        state=false;
                        button.setText("WYCZYSC");
                    }
                }
                else
                {
                    findtext.removehighlights(editorArea);
                    state=true;
                    button.setText("SZUKAJ");
                }
            } 
            catch (BadLocationException ex) 
            {
                Logger.getLogger(EditorWindow.class.getName()).log(Level.SEVERE, null, ex);
            }
       
        }
        });
         
      button_2.addActionListener(new ActionListener() {//Okodowanie przycisku POLACZ
        public void actionPerformed(ActionEvent e) {
           
           comunication_state=serialport.connect();
            label3.setText(comunication_state);
            
        }
        });
        
      button_3.addActionListener(new ActionListener() {//Okodowanie przycisku SPRAWDZ
        public void actionPerformed(ActionEvent e) {
           
           
           gcode_veryfication.check_gcode();
           
        }
        });    
      button_4.addActionListener(new ActionListener() {//Okodowanie przycisku ROZLACZ
        public void actionPerformed(ActionEvent e) {
           
            comunication_state=serialport.disconnect();
            label3.setText(comunication_state);
         
        }
        });
         
      button_5.addActionListener(new ActionListener() {//Okodowanie przycisku WYSLIJ
        public void actionPerformed(ActionEvent e) {
           
           
            serialport.writeData();
            
           
        }
        });
       
        
        serialport.searchPorts();
        combo.setFont(font);
        ((JLabel)combo.getRenderer()).setHorizontalAlignment(SwingConstants.CENTER);
       
        
        
        //dodanie elementów do zagniezdzonego layouta 
        JPanel pane2=new JPanel();
        GridLayout box=new GridLayout(12,1);
        pane2.setLayout(box);
        pane2.add(label);
        pane2.add(combo);
        pane2.add(label11);
        pane2.add(button_2);
        pane2.add(label12);
        pane2.add(button_4);
        pane2.add(label10);
        pane2.add(button_3);
        pane2.add(label13);
        pane2.add(button_5);
        pane2.add(label2);
        pane2.add(label3);
        pane2.add(label4);
        pane2.add(label5);
        pane2.add(label6);
        pane2.add(label7);
        pane2.add(label8);
        pane2.add(label9);
        
        pane2.add(textfield);
        pane2.add(button);
        pane.add(pane2,BorderLayout.EAST);
        //Koniec kodu odpowiedzalnego za zagniezdzony layout i elementy w jego wnetrzu 
        //pane.add(this.textfield, BorderLayout.PAGE_END);
       // this.findtext=new FindText(buttom,textfield,editorArea);
        /*JTextPane outputField = new JTextPane();
        outputField.setEditable(false);
        this.outputScrollPane = new JScrollPane(outputField);
        this.outputScrollPane.setVerticalScrollBarPolicy(
                        JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        this.outputScrollPane.setPreferredSize(new Dimension(800, 175));
        this.outputScrollPane.setMinimumSize(new Dimension(800, 100));
        */
       
    }
    
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws ClassNotFoundException, InstantiationException, IllegalAccessException, UnsupportedLookAndFeelException
    {
        EditorWindow mainWindow = new EditorWindow();
        mainWindow.addComponents(mainWindow.getContentPane());
        mainWindow.pack();
        mainWindow.setVisible(true);
    }
}
