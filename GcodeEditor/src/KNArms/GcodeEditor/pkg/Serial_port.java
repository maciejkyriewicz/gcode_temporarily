/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KNArms.GcodeEditor.pkg;

import gnu.io.*;
import java.awt.Color;
import java.awt.List;
import static java.awt.SystemColor.window;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.TooManyListenersException;
import javax.swing.JComboBox;
import javax.swing.JTextArea;

/**
 *
 * @author Maciej Tkacz
 */
public class Serial_port implements SerialPortEventListener
{
     
    private Enumeration ports = null;
    private HashMap portMap = new HashMap();
    private JComboBox combobox;
    private CommPortIdentifier selectedPortIdentifier = null;
    private gnu.io.SerialPort serialPort = null;
    private InputStream input = null;
    private OutputStream output = null;
    private boolean bConnected = false;
    private static final int DATA_RATE =9600;
    private String temp_1,state;
    private byte singleData;
    public byte[] buffer = new byte[2];
    String logText = "";
    String messageString="KR";
    private byte[]buffer_2=new byte[2];
    final static int TIMEOUT = 2000;
    private int licznik=0;
    private char[] stringToCharArray ;
    private char[] buffer_32=new char[32] ;
    private String string_text;
    private int count=0,count_2;
    private char header;
    private int header_int;
    private int divide,divide_2,k=0;
   
        
    public Serial_port(JComboBox combobox,JTextArea text)
    {
        this.combobox=combobox;
        buffer_2[0]=0x4B;
        buffer_2[1]=0x52;
        string_text=text.getText();
        this.stringToCharArray = string_text.toCharArray();
        
        
        
    }
    
     public void searchPorts()
    {
        ports = CommPortIdentifier.getPortIdentifiers();

        while (ports.hasMoreElements())
        {
            CommPortIdentifier curPort = (CommPortIdentifier)ports.nextElement();

            if (curPort.getPortType() == CommPortIdentifier.PORT_SERIAL)
            {
                combobox.addItem(curPort.getName());
               portMap.put(curPort.getName(), curPort);
            }
        }
    }
     public String connect()
    {
      
        String selectedPort = (String)combobox.getSelectedItem();
        selectedPortIdentifier = (CommPortIdentifier)portMap.get(selectedPort);

        CommPort commPort = null;

        try
        {
            
            commPort = selectedPortIdentifier.open("TigerControlPanel", TIMEOUT);
            serialPort = (gnu.io.SerialPort)commPort;
            serialPort.setSerialPortParams(DATA_RATE,SerialPort.DATABITS_8,SerialPort.STOPBITS_1,SerialPort.PARITY_NONE);
            setConnected(true);
            logText = selectedPort + " opened successfully.";
            state=logText;

        }
        catch (PortInUseException e)
        {
            logText = selectedPort + " is in use. (" + e.toString() + ")";
            state=logText;
            
        }
        catch (Exception e)
        {
            logText = "Failed to open " + selectedPort + "(" + e.toString() + ")";
            state=logText;
        }
        
        initListener();
        initIOStream();
        return logText;
    }
       final public boolean getConnected()
    {
        return bConnected;
    }

    public void setConnected(boolean bConnected)
    {
        this.bConnected = bConnected;
    }
    
    public boolean initIOStream()
    {
        boolean successful = false;

        try {
            //
            input = serialPort.getInputStream();
            output = serialPort.getOutputStream();
            
            serialPort.notifyOnDataAvailable(true);
            successful = true;
            return successful;
        }
        catch (IOException e) {
            logText = "I/O Streams failed to open. (" + e.toString() + ")";
            
            return successful;
        }
    }
     public void initListener()
    {
        try
        {
            serialPort.addEventListener(this);
            serialPort.notifyOnDataAvailable(true);
        }
        catch (TooManyListenersException e)
        {
            logText = "Too many listeners. (" + e.toString() + ")";
           
        }
    }
     public String disconnect()
    {
       
        try
        {
            serialPort.removeEventListener();
            serialPort.close();
            input.close();
            output.close();
            setConnected(false);
            logText = "Disconnected."; 
            
        }
        catch (Exception e)
        {
            logText = "Failed to close " + serialPort.getName() + "(" + e.toString() + ")";
        }
        return logText;
    }
     
    /**
     *
     * @param evt
     */
    @Override
     public void serialEvent(SerialPortEvent evt) {
         
        if (evt.getEventType() == SerialPortEvent.DATA_AVAILABLE)
        {  
            try 
            {
                 
                   //przykladowy bufor
                  
                   singleData =(byte)serialPort.getInputStream().read();
               
                   buffer[licznik]=singleData;
                   licznik=licznik+1;
                   if(licznik==2)
                   {
                       for (int i=0;i<2;i++)
                       {
                            System.out.print(new String(new byte[] {buffer[i]}));

                       }  
                       System.out.println();
                       licznik=0;
                        switch (count)
                         {
                            case 1:
                                header_int = stringToCharArray.length;
                                BigInteger bigInt = BigInteger.valueOf(header_int);
                                output.write(bigInt.toByteArray());
                                count++;
                            break;
                            case 2:
                                
                                divide = (stringToCharArray.length)/32;
                                divide_2 = stringToCharArray.length%32;
                                if (count_2<=divide)
                                {
                                    for ( int i=0 ;i<32;i++)
                                    { 
                                        buffer_32[i] = stringToCharArray[i+k];
                                    }
                                    k=k+32;
                                    count_2++;
                                   // BigInteger bigInt_2 = BigInteger.valueOf(buffer_32);
                                   // output.write(buffer_32);
                                }
                                else 
                                {
                                    for (int i=0;i<divide_2;i++)
                                    {
                                        output.write(buffer_32[i]); 
                                        k=0;
                                        count=0;
                                        count_2=0; 
                                    }
                                }
                             break;
           
                        }
            
                  
                   }
                   
                   
            }
            catch (IOException e)
            {   
                
            }
        }
    }
     public void writeData() 
    {
        try
        {
          output.write(buffer_2);
          count=1;
          

        }
        catch (Exception e)
        {
            logText = "Failed to write data. (" + e.toString() + ")"; 
        }
    }
     public String getValue()
     {
         
         return temp_1;
     }
     
}
